import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { themeColor, backgroundColor, statusBarHeight } from './src/Style';
import Navigator from './src/Navigator';

console.disableYellowBox = true;

export default class App extends React.Component {
  render() {
    return (
      <View style={{flex:1}}>
        <View style={{backgroundColor:themeColor,height:statusBarHeight}}/>
        <Navigator />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
