import { StyleSheet, Dimensions, Platform } from 'react-native';
function getStatusBarHeight() {
  let d = Dimensions.get('window');
  const { height, width } = d;

  if (Platform.OS !== 'ios') return 0;

  return (height === 812 || width === 812) ? 30 : 20;
}
export const statusBarHeight = getStatusBarHeight();Platform.OS === 'ios' ? 20 : 0;
export const themeColor = 'rgba(75,0,130, 0.9)';
export const themeColorOpaque = 'rgba(75,0,130, 0.65)';
export const backgroundColor = '#F5F5F5';
