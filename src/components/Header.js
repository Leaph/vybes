import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet, View, Text, Dimensions
} from 'react-native';
import { themeColor, backgroundColor, themeColorOpaque } from '../Style';

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>{this.props.data}</Text>
        <View style={styles.separator} />
      </View>
    );
  }
}

Header.propTypes = {
    data: PropTypes.string,
};
Header.defaultProps = {
};
export default Header;

const styles = StyleSheet.create({
  container: {
    justifyContent:'center',
    alignItems:'center',
    marginVertical:10
  },
  text: {
    fontSize:25,
    fontWeight:'bold'
  },
  separator: {
    height:3,
    width:Dimensions.get('window').width*.35,
    backgroundColor:themeColorOpaque
  }
});
