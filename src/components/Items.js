import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet, View, Text, TouchableOpacity, Image, FlatList, Dimensions, Animated
} from 'react-native';
import { themeColor, backgroundColor, themeColorOpaque } from '../Style';
import NavigationService from '../services/NavigationService';

class Items extends Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }
  renderItem = ({item, index}) => {
    return (
      <TouchableOpacity style={[styles.container, styles.shadow, {width:150}]} onPress={() => NavigationService.navigate('Item', {id: item.id})}>
        <Image source={{uri: item.picture_url}} style={{width:150, height:200}}/>
        <View style={{backgroundColor:backgroundColor}}>
          <View style={{margin:10}}>
            <Text style={styles.description} numberOfLines={2} ellipsizeMode={'tail'}>{item.desc}</Text>
            <Text style={styles.price}>S${item.price}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
  renderDetailed = ({item, index}) => {
    return (
      <TouchableOpacity style={[styles.container, {marginTop:10,width:Dimensions.get('window').width}]} onPress={() => NavigationService.navigate('Item', {id: item.id})}>
        <Image source={{uri: item.picture_url}} style={{width:Dimensions.get('window').width-20, height:400, marginHorizontal: 10}}/>
        <View style={{backgroundColor:backgroundColor}}>
          <View style={{margin:10}}>
            <Text style={[styles.description, {flexWrap:'wrap'}]}>{item.desc}</Text>
            <Text style={styles.price}>S${item.price}</Text>
            <View style={{flexDirection:'row'}}>
              <Text style={{fontSize:15,backgroundColor:themeColorOpaque,color:'white'}}>Only {item.qty} left</Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
  render() {
    const { data, detailed, header } = this.props;
    const props = {
      horizontal: !detailed,
      ListHeaderComponent: header,
      data: data,
      renderItem: detailed ? this.renderDetailed : this.renderItem,
      ItemSeparatorComponent: () => <View style={{width:10,backgroundColor:'white'}} />,
      contentContainerStyle: {
        backgroundColor:backgroundColor,
      }
    }
    return (
      <FlatList
        {...props}
      />
    );
  }
}

Items.propTypes = {
    data: PropTypes.array,
    detailed: PropTypes.boolean,
    header: PropTypes.element
};
Items.defaultProps = {
};
export default Items;

const styles = StyleSheet.create({
  container: {
    marginBottom:5
  },
  shadow: {
    shadowColor: "rgba(170,170,170,1)",
    shadowOpacity: 0.8,
    shadowRadius: 2,
    shadowOffset: { height: 1, width: 0 },
    elevation: 2
  },
  description: {
    fontSize:15
  },
  price: {
    color:themeColorOpaque,
    fontSize:16
  }
});
