import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet, View, Text, ImageBackground, Dimensions, TouchableOpacity
} from 'react-native';
import NavigationService from '../services/NavigationService';
import { themeColor, backgroundColor } from '../Style';

class Cover extends Component {
  render() {
    return (
      <ImageBackground
        source={{uri: this.props.url}}
        style={{width:Dimensions.get('window').width,height:300}}>
        <View style={{backgroundColor: this.props.children?'rgba(0,0,0,.6)':'transparent'}}>
          <TouchableOpacity style={{margin:5,width:65,backgroundColor:'rgba(255,255,255,0.7)',borderRadius:5,justifyContent:'center',alignItems:'center'}} onPress={() => NavigationService.goBack()}>
            <Text style={{padding:5,fontSize:18,color:themeColor}}>⟨Back</Text>
          </TouchableOpacity>
        </View>

        {!!this.props.children &&
          <View style={{flex: 1, backgroundColor: 'rgba(0,0,0,.6)',}}>
            {this.props.children}
          </View>
        }
      </ImageBackground>
    );
  }
}

Cover.propTypes = {
    source: PropTypes.string,
    children: PropTypes.element
};
Cover.defaultProps = {
};
export default Cover;
