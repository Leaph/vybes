import React, { Component } from 'react';
import {
  StyleSheet, View, Text, TouchableOpacity, Image,
  FlatList, Dimensions, ActivityIndicator, AppState,
} from 'react-native';
import { themeColor, backgroundColor, themeColorOpaque } from '../Style';
import NavigationService from '../services/NavigationService';
import { fetchListing } from '../services/DataProvider';
import Header from '../components/Header';
import Items from '../components/Items';

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      data: [],
    }
  }
  componentDidMount() {
    this.refresh();
  }
  refresh = () => {
    this.setState({isLoading: true});
    fetchListing().then(result => {
      this.setState({data: result, isLoading: false});
    });
  }
  renderSection({item}) {
    return (
      <View style={{flex: 1, marginVertical:10, paddingTop:10, paddingBottom:25, backgroundColor:'white', paddingHorizontal:10}}>
        <View style={{flexDirection:'row', alignItems:'center',marginHorizontal:10}}>
          <Image source={{uri: item.user.data.profile_picture}} style={{borderRadius:25,width:50,height:50}}/>
          <View style={{marginLeft:5}}>
            <Text style={{fontSize:20, fontWeight:'bold'}}>{item.user.data.instagram.username}</Text>
            <Text style={{fontSize:15}}>
              ♥ {item.user.data.instagram.counts.followed_by}
            </Text>
          </View>
        </View>

        <View style={{flexDirection:'row', paddingVertical:10}}>
          <Text style={{fontSize:15, width:Dimensions.get('window').width*.78}} numberOfLines={2} ellipsizeMode={'tail'}>{item.user.data.instagram.bio}</Text>
          <TouchableOpacity onPress={() => NavigationService.navigate('User', {items: item.items, user: item.user})}>
            <Text style={{fontSize:15, fontWeight:'bold', color: themeColorOpaque, width:Dimensions.get('window').width*.22}}>
              See All ⟩
            </Text>
          </TouchableOpacity>
        </View>

        <Items data={item.items} />

      </View>
    )
  }
  render() {
    const content = this.state.isLoading ? (
      <ActivityIndicator style={{margin: 16}}/>
    ) : (
      <FlatList
        ListHeaderComponent={<Header data={'EXPLORE'} />}
        data={this.state.data}
        renderItem={this.renderSection}
        contentContainerStyle={{}}
        onRefresh={this.refresh}
        refreshing={this.state.isLoading}
      />
    )
    return (
      <View style={{flex:1, backgroundColor:backgroundColor}}>
        {content}
      </View>
    );
  }
}

const styles = StyleSheet.create({
});
