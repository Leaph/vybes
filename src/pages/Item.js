import React, { Component } from 'react';
import {
  StyleSheet, View, Text, TouchableOpacity, Image,
  FlatList, Dimensions, ActivityIndicator, AppState,
} from 'react-native';
import { themeColor, backgroundColor, themeColorOpaque } from '../Style';
import { fetchItem } from '../services/DataProvider';
import Header from '../components/Header';
import Items from '../components/Items';
import Cover from '../components/Cover';

export default class User extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
    }
  }
  componentDidMount() {
    const { id } = this.props.navigation.state.params;
    fetchItem(id).then(result => {
      this.setState({item: result, isLoading: false});
    });
  }
  render() {
    if (this.state.isLoading) return (
      <ActivityIndicator style={{margin: 16}}/>
    )
    const { name, desc, picture_url, price, qty, user } = this.state.item;
    return (
      <View style={{flex:1, backgroundColor:'#F5F5F5',justifyContent:'space-between'}}>
        <View>
          <Cover url={picture_url} />

          <View style={{margin:10, justifyContent:'center'}}>
            <Text style={{fontSize:20, fontWeight:'bold'}}>{name}</Text>
            <View style={{flexDirection:'row'}}>
              <Text style={{fontSize:15}}>by </Text>
              <Text style={{fontSize:15, color:themeColorOpaque}}>{user.data.instagram.username}</Text>
            </View>
            <View style={{flexDirection:'row'}}>
              <Text style={{fontSize:15, color:themeColorOpaque}}>S${price}{'\t'}</Text>
              <Text style={{fontSize:15,backgroundColor:themeColorOpaque,color:'white'}}>Only {qty} left</Text>
            </View>
            <Text style={{fontSize:15,flexWrap:'wrap'}}>{desc}</Text>
          </View>
        </View>

        <TouchableOpacity style={{
          height:50,width:Dimensions.get('window').width,backgroundColor:themeColorOpaque,
          justifyContent:'center',alignItems:'center'}}>
          <Text style={{fontSize:18,color:'white'}}>Purchase</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
});
