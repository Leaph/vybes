import React, { Component } from 'react';
import {
  StyleSheet, View, Text, TouchableOpacity, Image,
  FlatList, Dimensions, ActivityIndicator, AppState,
} from 'react-native';
import { themeColor, backgroundColor, themeColorOpaque, statusBarHeight } from '../Style';
import Header from '../components/Header';
import Items from '../components/Items';
import Cover from '../components/Cover';
import ParallaxScrollView from 'react-native-parallax-scroll-view';

export default class User extends Component {
  constructor(props) {
    super(props);
    const { items, user } = this.props.navigation.state.params;
    this.state = {
      items, user
    }
  }
  renderForeground = (user) => {
    return (
      <View>
        <Cover url={user.data.cover_picture}>
          <View style={{alignItems:'center',paddingHorizontal:5}}>
            <Image source={{uri: user.data.profile_picture}} style={{borderRadius:40,width:80,height:80,borderWidth:1,borderColor:'white'}}/>
            <Text style={{color:'white',fontSize:18,fontWeight:'bold'}}>{user.data.instagram.username}</Text>
            <Text style={{color:'white',fontSize:14}}>♥ {user.data.instagram.counts.follows}</Text>
            <Text style={{color:'white',fontSize:14,flexWrap:'wrap',textAlign:'center'}}>{'\n' + user.data.instagram.bio}</Text>
          </View>
        </Cover>
        <Header data={'ITEMS'} />
      </View>
    )
  }
  renderStickyHeader = (user) => {
    return (
      <View style={{justifyContent:'center', alignItems:'center'}}>
        <Text style={{color: 'white', fontSize: 16, fontWeight:'bold'}}>{user.data.instagram.username}</Text>
      </View>
    )
  }
  render() {
    const { items, user } = this.state;
    return (
      <ParallaxScrollView
        backgroundColor={themeColor}
        contentBackgroundColor={backgroundColor}
        parallaxHeaderHeight={300}
        renderForeground={() => this.renderForeground(user)}
        renderStickyHeader={() => this.renderStickyHeader(user)}
        stickyHeaderHeight={30}
      >
      <View style={{flex:1, backgroundColor:backgroundColor}}>
        <View style={{flex:1}}>
          <Items data={items} detailed={true} />
        </View>
      </View>
    </ParallaxScrollView>
    );
  }
}

const styles = StyleSheet.create({
});
