const VYBES = 'https://api.getvybes.co';

export const fetchListing = () => {
  let url = VYBES+'/listing';
  return fetch(url, { method: 'GET' })
  .then((response) => response.json())
  .then((json) => json);
}
export const fetchUser = (id) => {
  let url = VYBES+'/members/'+id;
  return fetch(url, { method: 'GET' })
  .then((response) => response.json())
  .then((json) => json);
}
export const fetchItem = (id) => {
  let url = VYBES+'/listing/items/'+id;
  return fetch(url, { method: 'GET' })
  .then((response) => response.json())
  .then((json) => json);
}
