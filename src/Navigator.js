import React, { Component } from 'react';
import { StackNavigator } from 'react-navigation';
import NavigationService from './services/NavigationService';
import Home from './pages/Home';
import User from './pages/User';
import Item from './pages/Item';

export default class App extends Component {
  render() {
    const StackNav = StackNavigator({
        Home: {screen: Home},
        User: {screen: User},
        Item: {screen: Item},
    }, {navigationOptions:{ header: null }});
    return (
      <StackNav ref={navigatorRef => {
        NavigationService.setContainer(navigatorRef);
      }}/>
    );
  }
}
